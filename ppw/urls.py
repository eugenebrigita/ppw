from django.urls import path, include
import story_1.views
import portfolio.views
from django.contrib import admin

admin.autodiscover()

app_name = 'portfolio'



# To add a new path, first import the app:
# import blog
#
# Then add the new path:
# path('blog/', blog.urls, name="blog")
#
# Learn more here: https://docs.djangoproject.com/en/2.1/topics/http/urls/

urlpatterns = [
    path("", portfolio.views.intro, name="intro"),\
    path("home", portfolio.views.home, name="home"),
    path("about", portfolio.views.about, name="about"),
    path("experience", portfolio.views.experience, name="experiences"),
    path("blog", portfolio.views.blog, name="blogs"),
    path("contact", portfolio.views.contact, name="contact"),
    path("story_1", story_1.views.index, name="index"),\
    path("admin/", admin.site.urls),
    path("form/", portfolio.views.form, name="form"),
    path("show/", portfolio.views.show, name="show"),
    path("remove_activity/(?P<pk>\d+)", portfolio.views.remove_activity, name="remove_activity"),
]
