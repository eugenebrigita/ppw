from django.db import models

# Create your models here.


class ActivityModel(models.Model):
    name = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField(auto_now_add=False)
    event = models.CharField(max_length=50)
    note = models.CharField(max_length=130)