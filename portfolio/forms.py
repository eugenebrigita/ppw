from django.db import models
from django import forms
from .models import ActivityModel

import datetime


class ActivityForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs= {
            'class': 'form-control',
            'placeholder': 'tell me, what is your name?',
            'size': '65%',
        }
    ))
    date = forms.DateField(input_formats=['%Y/%m/%d'], widget=forms.DateInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'fill the date with format YYYY/MM/DD',
        }
    ))
    time = forms.TimeField(widget=forms.TimeInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'fill the time with format HH:MM',
        }
    ))
    event = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'the thing is ...'
        }
    ))
    note = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'what should be noted by Eugene?'
        }
    ))

    class Meta:
        model = ActivityModel
        fields = (
            "name",
            "date",
            "time",
            "event",
            "note",
        )

        labels = {
            "name": "Name",
            "date": "Date",
            "time": "Time",
            "event": "Event",
            "note": "Note",
        }