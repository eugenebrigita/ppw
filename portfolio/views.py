from django.shortcuts import render
from django.http import HttpResponse
from .models import ActivityModel
from .forms import ActivityForm
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages

def intro(request):
    return render(request, "intro.html")

def home(request):
    return render(request, "home.html")

def about(request):
    return render(request, "about.html")

def experience(request):
    return render(request, "experience.html")

def blog(request):
    return render(request, "blog.html")

def contact(request):
    return render(request, "contact.html")

def show(request):
    data = ActivityModel.objects.all()
    return render(request, 'form-content.html', {'data': data})

def form(request):
    if request.method == "POST":
        data = ActivityForm(request.POST or None)
        if data.is_valid():
            data.save()
            return redirect('show')
        else:
            messages.warning(request, "Oops, there's invalid! \n Please re-input peeps!")
    else:
        data = ActivityForm()
    return render(request, 'form.html', {'form': data})

def remove_activity(request, pk):
    activity = get_object_or_404(ActivityModel, pk=pk)
    activity.delete()
    return redirect('show')
    #
    # form = ActivityForm(instance=activity)
    #
    # context = {
    #     'form': form,
    #     'activity': activity
    # }